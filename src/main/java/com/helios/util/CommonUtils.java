package com.helios.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonUtils {
	WebDriver driver;
	
	public CommonUtils(WebDriver driver) {
		this.driver = driver;
	}

	public void navigateToUrl(String URL) {
		driver.navigate().to(URL);
	}
	
	public void waitTillElementPresent(WebElement element) {
		
		try {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		}
		catch(Throwable t) {
			//reportOutput("An error has occurred while executing waitForElementClickable keyword "+t.getMessage());
			//log.debug("An error has occurred while executing waitForElementClickable keyword "+t.getMessage());
		}
	}
	
	
}
