package com.helios.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Functions {
	FileInputStream fs = null;
	Properties prop = null;
	
	@SuppressWarnings("null")
	public Properties loadConfigFile(String folderName, String fileName) throws IOException {
		if (folderName.equalsIgnoreCase("config")) {
			folderName = System.getProperty("usr.dir")+"/src/main/resources/config";
		}
		fs = new FileInputStream(folderName +"/"+fileName+".properties");
		prop.load(fs);
		return prop;
	}
	
	public String getDriverPath(Properties properties, String browser) {
		String driverPath = properties.getProperty(browser+".driverpath");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");		
	}
	
	public String getURL(Properties properties, String path) {
		String driverPath = properties.getProperty(path+".url");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");		
	}
	
}
