package com.helios.managers;

import org.openqa.selenium.WebDriver;

import com.helios.pageobjects.BookATestDrive_page;
import com.helios.pageobjects.HomePage;
import com.helios.util.CommonUtils;

public class PageObjectManager {
	
	private WebDriver driver;
	private HomePage homepage;
	private BookATestDrive_page bookatestdrive;
	private CommonUtils commonUtils;

	public PageObjectManager(WebDriver driver) {
		this.driver = driver;
	}
	
	public HomePage getHomePage() {
		return (homepage == null) ? new HomePage(driver) : homepage;
	}
	
	public BookATestDrive_page getBookATestDrive_page() {
		return (bookatestdrive == null) ? new BookATestDrive_page(driver) : bookatestdrive;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public HomePage getHomepage() {
		return homepage;
	}

	public BookATestDrive_page getBookatestdrive() {
		return bookatestdrive;
	}

	public CommonUtils getCommonUtils() {
		return (commonUtils == null) ? new CommonUtils(driver) : commonUtils;
	}
	
	

}
