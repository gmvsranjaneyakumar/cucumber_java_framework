package com.helios.managers;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

import com.helios.util.Functions;

public class WebDriverManager {
	private static Functions functions;
	public static Properties testCONFIG;
	public static Properties urlCONFIG;
	private String browser;
	private static WebDriver driver;
	private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
	private static final String FIREFOX_DRIVER_PROPERTY = "webdriver.gecko.driver";
	
	static {
		try {
			testCONFIG = functions.loadConfigFile("config", "ConfigData");
			urlCONFIG = functions.loadConfigFile("config", "UrlInfo");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public WebDriverManager(String browser){
		this.browser = browser;
	}
	
	public WebDriver getDriver() {
		if(driver == null) driver = launchBrowser();
		return driver;
	}
		
	public WebDriver launchBrowser() {
		switch(browser.toLowerCase().trim()) {
		case ("firefox"):{
			System.setProperty(FIREFOX_DRIVER_PROPERTY,testCONFIG.getProperty(browser));
			driver = new FirefoxDriver();
		}
		case ("chrome"):{
			System.setProperty(CHROME_DRIVER_PROPERTY,testCONFIG.getProperty(browser));
			driver = new FirefoxDriver();
		}
		case ("safari"): {
			driver = new SafariDriver();
		}
		}
		driver.manage().window().maximize();
		int implicitwait = Integer.parseInt(testCONFIG.getProperty("implicitlyWait"));
		driver.manage().timeouts().implicitlyWait(implicitwait, TimeUnit.SECONDS);
		return driver;
	}

	public void closeDriver() {
		driver.close();
		driver.quit();
	}
		
}

