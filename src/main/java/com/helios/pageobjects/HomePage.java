package com.helios.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	public HomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how =How.CSS, using = ".logo a img")
	private WebElement logo_in_home_page;

	@FindBy(how = How.CSS, using = ".c_010B.grid-row.bleed a[data-adobe-tagging=book-a-test-drive]")
	private WebElement batd_link;
	
	@FindBy(how = How.CSS, using = ".c_007-1.carousel-slide img")
	private List<WebElement> carousel;

	public WebElement getLogo_in_home_page() {
		return logo_in_home_page;
	}

	public void setLogo_in_home_page(WebElement logo_in_home_page) {
		this.logo_in_home_page = logo_in_home_page;
	}

	public WebElement getBatd_link() {
		return batd_link;
	}

	public void setBatd_link(WebElement batd_link) {
		this.batd_link = batd_link;
	}
	
	public void selectBookATestDriveLink() {
		batd_link.click();
	}

	public List<WebElement> getCarousel() {
		return carousel;
	}
	
	public void setCarousel(List<WebElement> carousel) {
		this.carousel = carousel;
	}
	
}
