package com.helios.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class BookATestDrive_page {

	public BookATestDrive_page(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.CSS, using = ".c_056")
	private WebElement book_a_test_drive;
	
	@FindBy(how = How.CSS, using = ".logo a>img")
	private WebElement logo_in_batd_page;

	public WebElement getBook_a_test_drive() {
		return book_a_test_drive;
	}

	public void setBook_a_test_drive(WebElement book_a_test_drive) {
		this.book_a_test_drive = book_a_test_drive;
	}

	public WebElement getLogo_in_batd_page() {
		return logo_in_batd_page;
	}

	public void setLogo_in_batd_page(WebElement logo_in_batd_page) {
		this.logo_in_batd_page = logo_in_batd_page;
	}
	
	public void selectBrandLogo() {
		logo_in_batd_page.click();
	}
	
}
