package com.helios.stepdefinitions;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.helios.managers.WebDriverManager;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	private WebDriverManager webDriverManager;
	public static WebDriver driver;
	public static String environment;
	public static String brand;
	public static String market;
	public static String browser;
	public static String dark;
	public static String remote;
	
	@Before
	public void beforeSteps() {
		System.out.println("in before steps");
		environment = System.getenv("environment");
		brand = System.getenv("brand");
		market = System.getenv("market");
		browser = System.getenv("browser");
		dark = System.getenv("dark");
		remote = System.getenv("remote");
		webDriverManager = new WebDriverManager(browser);
		driver = webDriverManager.getDriver();
	}
	
	@After
	public void afterSteps(Scenario scenario) {
		if(scenario.isFailed()) {
	        try {
	            scenario.write("Current Page URL is "+ driver.getCurrentUrl());
	            byte[] screenShot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
	            scenario.embed(screenShot, "img/png");
	        } catch (WebDriverException somePlatformsDontSupportScreenshots) {
	            System.err.println(somePlatformsDontSupportScreenshots.getMessage());
	        }
	        }
		webDriverManager.closeDriver();
	}

}
