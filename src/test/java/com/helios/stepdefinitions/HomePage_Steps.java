package com.helios.stepdefinitions;


import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.helios.managers.PageObjectManager;
import com.helios.pageobjects.HomePage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomePage_Steps {
	WebDriver driver;
	HomePage homepage;
	PageObjectManager pageObjectManager;
	
	public HomePage_Steps(){
		driver = Hooks.driver;
		pageObjectManager = new PageObjectManager(driver);
	}
	
	@Given("^I am on the Home page$")
	public void i_am_on_the_Home_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		homepage = pageObjectManager.getHomePage();
		
	}

	@Then("^I should see brand logo displayed$")
	public void i_should_see_brand_logo_displayed() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Assert.assertTrue(homepage.getLogo_in_home_page().isDisplayed(), "Brand Logo is not displayed in home page");
	}

	@When("^I click on batd link on home page$")
	public void i_click_on_batd_link_on_home_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		homepage.selectBookATestDriveLink();
		try { Thread.sleep(10000);}
		catch (InterruptedException e) {}
	}

	@Then("^I should navigate to Home page$")
	public void i_should_navigate_to_Home_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Assert.assertTrue(homepage.getCarousel().get(0).isDisplayed(), "Did not navigate to HomePage");
	}


}
