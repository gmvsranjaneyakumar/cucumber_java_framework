package com.helios.stepdefinitions;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.helios.managers.PageObjectManager;
import com.helios.pageobjects.BookATestDrive_page;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BookATestDrive_Steps {
	WebDriver driver;
	BookATestDrive_page bookatestdrive;
	PageObjectManager pageObjectManager;
	
	public BookATestDrive_Steps(){
		driver = Hooks.driver;
		pageObjectManager = new PageObjectManager(driver);
	}
	
	@Then("^I should see brand logo displayed in batd page$")
	public void i_should_see_brand_logo_displayed_in_batd_page() throws Throwable {
		bookatestdrive = pageObjectManager.getBookATestDrive_page();
		Assert.assertTrue(bookatestdrive.getBook_a_test_drive().isDisplayed(), "Did not navigate to BATD page");
	    // Write code here that turns the phrase above into concrete actions
		Assert.assertTrue(bookatestdrive.getLogo_in_batd_page().isDisplayed(), "Brand logo is not displayed in BATD page");
	}

	@When("^I click on brand logo from batd page$")
	public void i_click_on_brand_logo_from_batd_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		bookatestdrive.selectBrandLogo();
		try { Thread.sleep(10000);}
		catch (InterruptedException e) {}
	}

}
