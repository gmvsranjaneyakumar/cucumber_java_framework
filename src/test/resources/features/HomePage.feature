Feature: Homepage

  Scenario: Verify brand logo
    Given I am on the Home page
    Then I should see brand logo displayed
    When I click on batd link on home page
    Then I should see brand logo displayed in batd page
    When I click on brand logo from batd page
    Then I should navigate to Home page